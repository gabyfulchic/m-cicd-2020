# CI/CD TP1 Dév

<!-- vim-markdown-toc GitLab -->

* [First day](#first-day)
    * [Setup Gitlab](#setup-gitlab)
    * [Un bout de code ?](#un-bout-de-code-)
    * [Une première pipeline](#une-première-pipeline)
    * [Linting de code](#linting-de-code)
* [Second day](#second-day)
    * [Gitlab : Docker integration](#gitlab-docker-integration)
    * [Sonarqube](#sonarqube)

<!-- vim-markdown-toc -->

# First day

## Setup Gitlab

* création de compte
* création d'un dépôt de test

## Un bout de code ? 

Le mieux pour effectuer ensuite des tests d'intégration et effectuer du déploiement facilement ça serait d'avoir une petite app API REST. Utilisez le langage/framework de votre choix pour coder une API simpliste (une ou deux entités, avec du GET/POST/DELETE simplement).

## Une première pipeline

Dans l'interface de Gitlab, sur un dépôt donné, vous trouverez dans le volet de gauche un menu CI/CD, où vous verrez vos pipelines s'exécuter.

Afin de déclencher une pipeline, il faut un fichier `.gitlab-ci.yml` à la racine de votre dépôt git.

Exemple de structure :

```yaml
stages:
  - first_test

first-test:
  stage: first_test
  image: debian
  script:
    - echo 'toto'
```

Tester ce premier fichier `.yml`.

## Linting de code

Créer une nouvelle pipeline qui met en place du linting. Pour ce faire :
* trouver un binaire qui est capable de lint votre code (par exemple `pylint` pour Python)
* utiliser une image Docker qui contient ce binaire
* créer une pipeline qui mobilise cette image afin de tester votre code

# Second day

## Gitlab : Docker integration

Il est possible d'utiliser les pipelines de CI/CD pour construire et packager des images Docker. Ces images peuvent ensuite être utilisées à des fins de dév, ou poussées sur une infra de prod.

Gitlab embarque nativement un registre Docker (un registre permet d'héberger des images Docker, à l'instar du Docker Hub).

Lors du déroulement d'une pipeline, plusieurs variables d'environnement sont disponibles à l'intérieur du fichier `.gitlab-ci.yml`. On peut par exemple récupérer dynamiquement le nom de la branche qui est en train d'être poussée, l'ID de commit ou encore, l'adresse du registre Docker associé à l'instance Gitlab.

Pour tester tout ça, on va avoir besoin de deux fichiers :
* un Dockerfile
  * décrit comment construire une image Docker
* un `.gitlab-ci.yml` qui fait appel à des commandes `docker` pour construire et pousser les images sur le registre

---

Exemple de setup simple :
* [Dockerfile](./files/docker-integration/Dockerfile)
* [.gitlab-ci.yml](./files/docker-integration/.gitlab-ci.yml)

Testez le build et push automatique de conteneur :
* ajouter ces fichiers à un dépôt Gitlab
* une fois le push effectué et la pipeline terminée, un nouvel onglet est disponible lorsque vous consultez le projet depuis la WebUI `Packages & Registries > Container Registry`

Pour pousser un peu plus, vous pouvez créer un Dockerfile vous-même qui package dans une image Docker votre code et le pousse sur le registry. Si vous avez Docker sur votre machine, vous pourrez ensuite directement récupérer les images avec un `docker pull`.

## Sonarqube

Bon. Pour Sonarqube c'est compliqué d'utiliser l'instance publique gitlab.com et Sonarqube de concert si on a pas accès facilement à une IP publique pour tout le monde. 

L'alternative : un `docker-compose.yml` qui monte ce qu'il faut pour reproduire un environnement de travail similaire à Gitlab. Vous pouvez le lancer dans une VM pour avoir cette stack en local et utiliser Sonarqube.

Le `docker-compose.yml` est clairement inspiré du TP infra et embarque :
* Gitea
  * gère des dépôts gits
  * gestion de users/permissions
* Drone
  * Pipeline de CI
* Registre Docker
* Sonarqube
  * Analyse de code

---

Steps :
* installer Docker et `docker-compose` en suivant la documentation officielle
  * dans une VM Linux si vous pouvez
  * sur votre hôte c'est possible, je vous laisse gérer votre environnement :)
* récupérer le [docker-compose.yml](./files/sonarqube/docker-compose.yml) et le placer dans un répertoire de travail
* lancer la stack avec un `docker-compose up -d`
* ajouter une ligne à votre fichiers `hosts` pour faire pointer le nom `ci` vers l'IP de la machine qui porte la stack

Les différents WebUI sont dispos sur des ports différents :
* Gitea : `http://ci:3000`
* Drone : `http://ci`
* Sonarqube : `http://ci:8080`

Steps :
* aller sur la WebUI de Gitea
  * onglet `Explore`
  * laisser les paramètres par défaut **SAUF :** n'oubliez pas la création d'un utilisateur admin (tout en bas)
  * vous authentifier pour vérifier le bon fonctionnement
  * créer un dépôt et y pousser du code
* aller sur la WebUI de Sonarqube
  * se log avec `admin:admin`
  * aller dans les paramètre du compte `My Account` puis onglet `Security`
  * générer un token et notez le somewhere
* aller sur la WebUI de Drone
  * les credentials sont les mêmes que pour Gitea
  * déclencher une synchro avec Gitea
  * activer le dépôt et configurez-le :
    * cocher "Trusted"
    * définir deux variables :
      * `sonar_host` : `http://sonarqube:9000`
      * `sonar_token` : `<YOUR_TOKEN>`
* ajouter un `.drone.yml` à la racine de votre dépôt
  * [exemple ici](./files/sonarqube/.drone.yml)
* `git push` tout ça
* vous devriez voir votre code sur Gitea, testé dans Drone, analysé dans Sonarqube

Explorez une peu l'interface de Sonarqube une fois qu'il est en place, beaucoup de métriques intéressantes y remontent.

Drone est quant à lui un outil minimaliste mais puissant. Vous pouvez mettre en place une pipeline similaire à celle de Gitlab sous Drone.
